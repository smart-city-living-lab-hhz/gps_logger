# GPS_Logger

Im nachfolgendem werden wir euch zeigen, wie ihr euren eigenen GPS Logger verbauen könnt. Anschließend werden wir über Arduino den Code auf den GPS Logger draufspielen, sodass die einzelnen GPS-Koordinaten auf der SD Karte gespeichert werden, wenn dieser mit Strom versorgt wird. 

Ziel des GPS Loggers ist es, das Fahrzeug zu "tracken". Die einzelnen GPS-Koordinaten, die jede Sekunde auf der SD Karte gespeichert werden, verraten uns wo und wann das Fahrzeug unterwegs war. Es entsteht ein Datensatz mit den Koordinaten. Die Geschwindigkeit kann ebenfalls abgelest werden. Außerdem kann die gefahrene Strecke auf einer Karte visualisiert werden, sodass beispielsweise erkennbar ist, ob der Fahrer im Stau stand oder er mehrmals im gleichen Gebiet rumgefahren ist, da er beispielsweise keinen Parklplatz gefunden hat. 

Eine Beispielaufzeichnung ist in der Datei Beispielaufzeichnung.txt zu finden. 
Außerdem kann im Code festgelegt werden, dass die Aufzeichnung der Koordinaten nur in einem definierten Bereich stattfinden. Hierfür wird im Code in den Zeilen die GPS Koordinaten des gewünschten Gebietes eingegeben. Im folgenden Beispiel werden vom GPS Logger nur der folgende Bereich aufgezeichnet (Diezenhalde Böblingen):

![image](/Bilder/diezenhalde_box.png)

_Quelle: https://www.openstreetmap.de/karte.html Eigene Darstellung_

Die Punkte stellen dabei die sekündlichen Wegpunkte dar. 

Für den GPS Logger wurde ein Gehäuse mit dem 3D Drucker hergestellt. Die notwendige STL Datei zum Drucken sind gps_logger_oben_final.stl und gps_logger_unten_final.stl
Wir empfehlen beide Gehäuseteile mit Gummibändern zu fixieren. 
Der GPS Logger wird per USB-Strom betrieben und mit den 4 Saugnäpfen an die Windschutzscheibe des Fahrzeuges fixiert. 

![image](Bilder/GPS_Logger_Saugknöpfe.png)

SD Karte einsetzen, herausnehemen:

![image](/Bilder/VID-20220111-WA0006.gif)

Schauen wir uns nun die einzelnen Schritte an, damit ihr selber auch einen GPS Logger benutzen könnt. 

## **1. Benötigte Bauteile**

Die benötigten Bauteile werden im Folgendem mit den dazugehörigen Links aufgelistet, damit ihr sie direkt selber bestellen könnt:

1. ESP32: https://www.berrybase.de/dev.-boards/esp8266-esp32-d1-mini/boards/esp32-nodemcu-development-board

2. NEO-6M inkl. Antenne: https://www.berrybase.de/audio-video/navigation/u-blox-neo-6m-gps-ttl-empf-228-nger-inkl.-antenne

3. SD Card Module: https://www.berrybase.de/neu/micro-sd-card-reader-modul-mit-spi-schnittstelle?c=2507

5. 10 Verbindungs-Kabel: https://www.berrybase.de/raspberry-pi/raspberry-pi-computer/kabel-adapter/gpio-csi-dsi-kabel/40pin-jumper/dupont-kabel-female-150-female-trennbar

6. Eine 5mm LED welche die erfolgreiche GPS Verbindung mit durchgendes Leuchten visualiert. 

7. Saugnäpfe für das Gehäuse (20mm): https://www.amazon.de/transparent-suction-plastic-without-bathroom/dp/B0919WNVWG/ref=sr_1_16?currency=EUR&keywords=saugnapf&language=de_DE&qid=1641902532&sr=8-16 

Zusätzlich benötigt ihr eine Micro SD Karte, die ihr in das SD Card Module reinstecken könnt. Hierfür empfehlen wir eine SanDisk: https://www.berrybase.de/raspberry-pi/raspberry-pi-computer/speicherkarten/sandisk-ultra-microsdhc-a1-98mb/s-class-10-speicherkarte-43-adapter-16gb


## **2. Bauteile zusammenbauen**

In der unteren Abbildung und zusätzlich in den zwei Tabellen könnt ihr dann sehen, wie ihr die Bauteile miteinander verbinden müsst, damit ihr einen funktionierenden GPS Logger besitzt. Die Antenne wird im NEO-6M verbaut. 

![image](/uploads/9e88dcde9b1fd6a6cdd71eac8db0d69c/image.png)

ESP32  | SD Card Module                 
------------- | -------------
Pin 5  | CS
Pin 18  | SCK
Pin 23  | MOSI
Pin 19  | MISO
5V  | VCC
GND  | GND

ESP32  | NEO-6M
------------- | -------------
3V3  | VCC
Pin 17  | RX
Pin 16  | TX
GND  | GND

## **3. Code über Arduino draufspielen**

Nachdem alle Bauteile, wie im Bild angeschlossen sind, muss nur noch der Code auf den GPS Logger draufgespielt werden. Dazu benötigen wir das Programm Arduino, welches ihr euch unter folgendem Link runterladen könnt. 

Arduino: https://www.arduino.cc/en/software 

Nun könnt ihr das Programm öffnen und euren GPS Logger mit dem PC per USB verbinden. Anchließend kopiert ihr den Code mit dem der GPS_Logger bespielt werden soll aus dem Ordner [Code](https://gitlab.reutlingen-university.de/smart-city-living-lab-hhz/gps_logger/-/tree/main/Code) und fügt diesen in Arduino ein.

Als nächstes müsst ihr die nötigen Bibliotheken in das Programm einbinden. Die folgende Abbildung zeigt welche Bibliotheken ihr dafür installieren müsst.

![image](/uploads/2a84dca39cdc7e2b44fa56d370e84c13/image.png)

Wollt ihr nun beispielsweiße die Bibliothek "SPI" installieren geht ihr wie folgt vor.

1. Klickt auf Sketch und anschließend auf Bibliothek einbinden. 
2. Klickt nun auf Bibliotheken verwalten. Daraufhin öffnet sich der Bibliotheksverwalter.
3. Sucht nun in der Suchleiste nach der Bibliothek. Hier "SPI"
4. Wählt die gefundene Bibliothek aus und klickt auf installieren.

Habt ihr alle benötigten Biliotheken installiert, solltet ihr den Code überprüfen. Dafür klickt ihr auf das Häkchen oben links.

![image](/uploads/0f64de807a7ec566b8b472dc00b2a2c6/image.png)

Falls alles richtig installiert wurde, läuft die Überprüfung durch den Code durch.

Bevor ihr den Code auf den GPS_Logger spielen könnt, müsst ihr noch bei "Board" unter dem Reiter "Werkzeuge" das "ESP32 Dev Module" auswählen. Falls dieses Board nicht angezeigt wird, müsst ihr die ESP32 Module über den Boardverwalter installieren.

![image](/uploads/fc992d3afc99ef4cd78043841e99ba9e/image.png)

Anschließend könnt ihr den GPS_Logger am PC anschließen und diesen über den Button hochladen mit dem Code bespielen.

![image](/uploads/171698f0a110e933715b4715abb24e32/image.png)

Falls das Hochladen erfolgreich war, wird dies angezeigt. Nun könnt ihr euren GPS Logger verwenden, um eure GPS Daten zu tracken. Wenn ihr diese GPS-Daten auch gerne analysieren wollt, empfehlen wir euch folgendes GitLab Repository. 

[GPS-Daten-Analyse](https://gitlab.reutlingen-university.de/smart-city-living-lab-hhz/gps-daten-analyse)
