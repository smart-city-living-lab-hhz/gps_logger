#include <TinyGPS++.h>
#include <SoftwareSerial.h>
#include "FS.h"
#include "SD.h"
#include <SPI.h>
//#include "EEPROM.h"

static const int RXPin = 16, TXPin = 17;
static const uint32_t GPSBaud = 9600;
const byte led_gpio = 21;
#define SD_CS 5
RTC_DATA_ATTR int readingID = 0;

String dataMessage;
String fileName = "leer"; 
String monat,tag,jahr;
String stunde,minute1,sekunde;
float flat, flong, kmh;
int sat; 

TinyGPSPlus gps;

HardwareSerial SerialGPS(1);

// -----------------------------------------------------

void setup() {

  pinMode(led_gpio, OUTPUT);
  Serial.begin(9600);
  SerialGPS.begin(9600, SERIAL_8N1, 16, 17); 
  SD.begin(SD_CS);  
  if(!SD.begin(SD_CS)) {
    Serial.println("Kartenmontage fehlgeschlagen");
    return;
  }
  uint8_t cardType = SD.cardType();
  if(cardType == CARD_NONE) {
    Serial.println("Keine SD-Karte eingesetzt");
    return;
  }
  Serial.println("SD-Karte initialisieren");
  if (!SD.begin(SD_CS)) {
    Serial.println("ERROR - Initialisierung der SD-Karte fehlgeschlagen!");
    return;    // init failed
  }
  
}

void loop() {
  // put your main code here, to run repeatedly:
  gps_delay(1000);
  Serial.print(gps.date.value());
  if(gps.date.value() > 0 && fileName!="leer"){
    Serial.println(fileName.c_str()); 
    loggeDaten(); 
  }else{
    erstelleFileName();
    erstelleFile();
  }
    
}

static void gps_delay(unsigned long ms)
{
  unsigned long start = millis();
  do 
  {
    while (SerialGPS.available())
      gps.encode(SerialGPS.read());
  } while (millis() - start < ms);
}

void erstelleFileName() {
  if(gps.date.value() > 0)
  {
    fileName = "/" + String(gps.date.value()) + ".txt"; 
    Serial.print("File Name war erfolgreich: ");
    Serial.println(fileName.c_str());
    Serial.print("Der Zeitstempel: ");
    Serial.println(gps.time.value());
  }
  Serial.print("Noch kein Zeitstempel!!! ");
  Serial.println(gps.date.value());  
}

void erstelleFile(){
  if(fileName != "leer"){
  File file = SD.open(fileName.c_str());
  if(!file) {
    Serial.println("Die Datei existiert nicht");
    Serial.println("Datei erstellen...");
    writeFile(SD, fileName.c_str(), "Reading ID, Tag, Monat, Jahr, Stunde, Minute, Sekunde, Latitude, Longitude, Speed, AnzahlSat \r\n");
  }
  else {
    Serial.println("Datei existiert bereits");  
  }
  file.close();  
  }
}

void leseGPS(){
   while (SerialGPS.available() > 0)
    if (gps.encode(SerialGPS.read()))

  if (millis() > 5000 && gps.charsProcessed() < 10)
  {
    Serial.println(F("Kein GPS erkannt: Verkabelung prüfen."));
    while(true);
  }
  
  tag = gps.date.day();
  monat = gps.date.month();
  jahr = gps.date.year();
  stunde = gps.time.hour();
  minute1 = gps.time.minute();
  sekunde = gps.time.second();
  flat = gps.location.lat();
  flong = gps.location.lng(); 
  kmh = gps.speed.kmph();
  sat = gps.satellites.value();

  Serial.print("Lat-Test:  ");
  Serial.println(gps.location.lat()); 
  if (gps.satellites.value() > 4){
    //LED an
     digitalWrite(led_gpio, HIGH);
  }else{
    //LED aus
    digitalWrite(led_gpio, LOW);
  }
}

void loggeDaten() {
  if (gps.date.isValid()){
    leseGPS();
    //nur wenn man innerhalb der Box ist wird aufgezeichnet 
    //Box Koordinaten Diezenhalde
    if((flong > 8.991658 && flong < 9.011137) && (flat < 48.674627 && flat > 48.664865)){
    dataMessage = String(readingID) + "," + String(tag) + "," + String(monat) + "," 
                + String(jahr) + "," + String(stunde) + "," + String(minute1) + "," 
                + String(sekunde) + "," + String(flat, 6) + "," + String(flong, 6) + "," + String(kmh) + "," + String(sat) + "\r\n";
  Serial.print("Speicher Datensatz: ");
  Serial.println(dataMessage);
  appendFile(SD, fileName.c_str(), dataMessage.c_str());
  readingID++;
  }else{
    Serial.print("---nicht in der Box---"); 
  }
      Serial.println(String(flat, 6));
      Serial.println(String(flong, 6));
    }
  }

// Write to the SD card (DON'T MODIFY THIS FUNCTION)
void writeFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Writing file: %s\n", path);

  File file = fs.open(path, FILE_WRITE);
  if(!file) {
    Serial.println("Failed to open file for writing");
    return;
  }
  if(file.print(message)) {
    Serial.println("File written");
  } else {
    Serial.println("Write failed");
  }
  file.close();
}

// Append data to the SD card (DON'T MODIFY THIS FUNCTION)
void appendFile(fs::FS &fs, const char * path, const char * message) {
  Serial.printf("Appending to file: %s\n", path);

  File file = fs.open(path, FILE_APPEND);
  if(!file) {
    Serial.println("Failed to open file for appending");
    return;
  }
  if(file.print(message)) {
    Serial.println("Message appended");
  } else {
    Serial.println("Append failed");
  }
  file.close();
}
